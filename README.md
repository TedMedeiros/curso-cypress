# Projeto básico de automação de testes com Cypress

O projeto consiste em fornecer uma base para treinamento inicial com o framework Cypress para automação de testes end-to-end. Toda implementação foi feita com base no curso [**Testes automatizados com Cypress (básico)**](https://talkingabouttesting.coursify.me/courses/testes-automatizados-com-cypress-basico) da escola **Talking About Testing**. 



## Instalação

Ao clonar o projeto basta executar o seguinte comando para instalar o **Cypress** e todas as dependências necessárias para a execução dos testes:

> npm install

## Execução dos testes

O Cypress oferece duas formas de execução dos testes: **interativa** e **headless**.

### Interativa
É aberto uma janela do browser onde é possível visualizar todas as ações executadas no teste.

comando:

> npx cypress open

### Headless
Os testes são executados sem a exibição do browser. Ao final da execução, é gerado um arquivo de vídeo com as ações executadas nos testes.

comando:

> npm test
